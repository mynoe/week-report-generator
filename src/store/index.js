import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
// 递归对象 生成一个格式
// let time = genReportTitle()
export default new Vuex.Store({
  state: {
    baseInfo: {
      department: 'xx科技-企业生态部',
      position: '前端开发',
      clarkName: '张超',
      // curentTime: time,
    },
    projectInfo: [
      {
        businessName: '', // 业务名称
        businessTarget: '', // 业务目标
        currentSituation: '', // 当前情况
        demonds: [
          {
            name: '',
            target: '',
            background: '',
            situation: '',
            progress: [
              {
                timePoint: '',
                desc: '',
              },
            ],
            todoList: [
              {
                timePoint: '',
                desc: '',
              },
            ],
            risks: [
              {
                timePoint: '',
                desc: '',
              },
            ],
          },
        ],
      },
    ],
    reportList: [],
    activeReportId: {
      id: '',
      title: ''
    }
  },
  mutations: {
    // 设置当前周报信息
    setCurWeekReport(state, payload) {
      console.log('setCurWeekReport---', payload)
      state.projectInfo = payload
    },
    //
    addNewProject(state, payload) {
      console.log('payload---', payload)
      const { projectInfo } = state
      projectInfo.push(payload)
      return {
        ...state,
        projectInfo,
      }
    },
    setReportList(state, payload) {
      console.log('setReportList---payload', payload)
      // const { data } = payload
      state.reportList = payload.data
      setTimeout(() => {
        state.activeReportId = {
          id: payload.data[0].id,
          title: payload.data[0].title
        }
      }, 1000)

    },
    setActiveReportId(state, payload) {
      state.activeReportId = payload
    }
  },
  actions: {},
  modules: {},
})
