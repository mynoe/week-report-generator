// import Vue from 'vue'

import TextSetter from './text-setter.vue'
import ListSetter from './list-setter.vue'
import NumberSetter from './number-setter.vue'
import ImgSetter from './img-setter.vue'
import CurrenceSetter from './currence-setter.vue'

const Setters = [
  {
    name: 'TextSetter',
    label: '文本项',
    icon: 'ios-brush-outline',
    component: TextSetter,
  },
  {
    name: 'ListSetter',
    label: '列表项',
    icon: 'ios-list',
    component: ListSetter,
  },
  {
    name: 'NumberSetter',
    label: '数值项',
    icon: 'social-tumblr-outline',
    component: NumberSetter
  },
  {
    name: 'ImgSetter',
    label: '图片项',
    icon: 'social-tumblr-outline',
    component: ImgSetter
  },
  {
    name: 'CurrenceSetter',
    label: '货币项',
    icon: 'social-tumblr-outline',
    component: CurrenceSetter
  }
]

export default Setters
