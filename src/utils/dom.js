// import $ from 'jquery'
export const siblings = (elm) => {
  var a = [] //保存所有兄弟节点
  let curentDom = elm.parentNode
  var p = elm.parentNode.parentNode.children //获取父级的所有子节点
  for (var i = 0; i < p.length; i++) {
    //循环
    if (p[i].nodeType == 1 && p[i] != curentDom) {
      //如果该节点是元素节点与不是这个节点本身
      a.push(p[i]) // 添加到兄弟节点里
    }
  }
  return a
}

//根据容器ID来渲染行内样式，避免长时间卡顿
export const translateStyle = (contentId) => {
  let { $ } = window
  const sheets = document.styleSheets
  const sheetsArry = Array.from(sheets)
  const $content = $('#' + contentId)
  // console.log('content', $content)
  sheetsArry.forEach((sheetContent) => {
    const { rules, cssRules } = sheetContent
    // console.log('sheetContent', sheetContent)

    //cssRules兼容火狐
    const rulesArry = Array.from(rules || cssRules || [])
    rulesArry.forEach((rule) => {
      const { selectorText, style } = rule
      // console.log('style-map', styleMap)
      //全局样式不处理
      if (selectorText !== '*') {
        //兼容某些样式在转换的时候会报错
        try {
          const select = $content.find(selectorText)
          select.each((domIndex) => {
            const dom = select[domIndex]
            let i = 0
            const domStyle = window.getComputedStyle(dom, null)
            // console.log('domStyle', domStyle)
            while (style[i]) {
              //样式表里的className是驼峰式，转换下便对应上了
              const newName = style[i].replace(/-\w/g, function (x) {
                return x.slice(1).toUpperCase()
              })
              $(dom).css(style[i], domStyle[newName])
              i++
            }
          })
        } catch (e) {
          // console.log('转换成行内样式失败')
        }
      }
    })
  })
}
