export const baseDemand = {
  name: '示例需求',
  desc: '示例需求描述',
  background: '示例需求背景',
  situation: '示例需求进展',
  progress: [
    {
      timePoint: '示例时间节点09-18',
      desc: '',
    },
  ],
  todoList: [
    {
      timePoint: '示例时间节点0909-20',
      desc: '沟通新需求',
    },
  ],
  risks: [
    {
      timePoint: '示例时间节点0909-23',
      desc: '环境配置问题待解决',
    },
  ],
}

export const baseProgress = {
  desc: '',
  timePoint: ''
}

export const baseTodo = {
  desc: '',
  timePoint: ''
}
export const baseRisk = {
  desc: '',
  timePoint: ''
}

export const baseReportContent = [
  {
    businessName: '', // 业务名称
    businessTarget: '', // 业务目标
    currentSituation: '', // 当前情况
    demonds: [
      {
        name: '',
        target: '',
        background: '',
        situation: '',
        progress: [
          {
            timePoint: '',
            desc: '',
          },
        ],
        todoList: [
          {
            timePoint: '',
            desc: '',
          },
        ],
        risks: [
          {
            timePoint: '',
            desc: '',
          },
        ],
      },
    ],
  },
]