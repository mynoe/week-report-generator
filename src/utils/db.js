
function DataBase(opts) {
  this.db = opts.db || {}
  this.objectStore = opts.objectStore || {}
  this.request = window.indexedDB.open(opts.databaseName, opts.version);
  // 初始化数据库
  this.init()
}

// 初始化数据库
DataBase.prototype.init = function () {
  console.log('db--this', this)
  let self = this
  return new Promise(() => {
    self.request.onerror = function (e) { console.log(e) }
    self.request.onsuccess = function (e) {
      console.log(e)
      self.db = self.request.result//可以拿到数据库对象
    }
    //如果指定的版本号，大于数据库的实际版本号，就会发生数据库升级事件upgradeneeded
    this.request.onupgradeneeded = function (event) {
      self.db = event.target.result;
      console.log('objectStoreNames', self.db.objectStoreNames)
      if (!self.db.objectStoreNames.contains('reports')) {//判断是否存在
        console.log('不存在reports')
        self.objectStore = self.db.createObjectStore('reports', { keyPath: 'id' });
        //自动生成主键db.createObjectStore(
        //  'person',
        //  { autoIncrement: true }
        //);  
      }
      //新建索引，参数索引名称、索引所在的属性、配置对象
      self.objectStore.createIndex('title', 'title', { unique: true });
    }
  })
}

// 新增周报数据

DataBase.prototype.addReport = function (content) {
  let request = this.db.transaction(['reports'], 'readwrite')
    .objectStore('reports')
    .add(content)

  request.onsuccess = function (err) {
    console.log('新增周报数据失败---', err)
  }
  request.onsuccess = function (succ) {
    console.log('新增数据成功----', succ)
  }
}
// 查询周报列表
DataBase.prototype.getReportList = function () {
  let self = this
  // return new Promise((resolve, reject) => {
  let res = {
    data: []
  }
  console.log('this.db', self)
  let objectStore = self.db.transaction('reports').objectStore('reports')
  // 成功打开游标的回调函数
  objectStore.openCursor().onsuccess = function (e) {
    let cursor = e.target.result;
    if (cursor) {
      // 将数据一条一条保存到arr中
      res.data.push(cursor.value);
      cursor.continue();
    } else {
      res.msg = '暂无更多数据'
      // reject(res)
    }
  }
  return res
  //   resolve(res)
  // })
}

// 查询周报数据
DataBase.prototype.searchById = function (id) {
  console.log(id)
  return new Promise((resolve, reject) => {
    let res = {
      data: {}
    }
    let request = this.db.transaction(['reports'], 'readwrite')
      .objectStore('reports')
      .get(id)

    request.onsuccess = function (err) {
      console.log('查询周报数据---', err)
      res.err = err
      reject(err)
    }
    request.onsuccess = function (e) {
      console.log('查询周报数据----', e.target.result)
      res.data = e.target.result
      resolve(res)
    }
  })
}

// 修改周报数据
DataBase.prototype.updateReport = function (content) {
  // console.log(id)
  return new Promise((resolve, reject) => {
    let res = {
      data: {}
    }
    let request = this.db.transaction(['reports'], 'readwrite')
      .objectStore('reports')
      .put(content)

    request.onsuccess = function (err) {
      console.log('修改周报数据---', err)
      res.err = err
      reject(res)
    }
    request.onsuccess = function (e) {
      console.log('修改周报数据----', e.target.result)
      res.data = e.target.result
      resolve(res)
    }
  })
}
// 删除周报数据
DataBase.prototype.delReport = function (id) {
  let self = this
  return new Promise((resolve, reject) => {
    var request = self.db.transaction(['reports'], 'readwrite')
      .objectStore('reports')
      .delete(id);

    request.onsuccess = function (event) {
      console.log('数据删除成功', event);
      resolve({
        code: 200,
        data: {},
        msg: '数据删除成功'
      })
    };
    request.onerror = function (err) {
      reject(err)
    }
  })
}

export default DataBase