import { DateUtil } from './week'

export const getMonthWeek = function (a, b, c) {
  /**
  * a = d = 当前日期
  * b = 6 - w = 当前周的还有几天过完(不算今天)
  * a + b 的和在除以7 就是当天是当前月份的第几周
  * 
  */
  let date = !a || !b ? new Date() : new Date(a, parseInt(b) - 1, c),
    w = date.getDay(),
    d = date.getDate();
  if (w == 0) {
    w = 7;
  }
  return {
    getMonth: date.getMonth() + 1,
    getYear: date.getFullYear(),
    getWeek: Math.ceil((d + 6 - w) / 7),
  }
};

export const getYearWeek = function (a, b, c) {
  /*
  date1是当前日期
  date2是当年第一天
  d是当前日期是今年第多少天
  用d + 当前年的第一天的周差距的和在除以7就是本年第几周
  */
  var date1 = !a || !b ? new Date() : new Date(a, parseInt(b) - 1, c),
    date2 = new Date(new Date().getFullYear(), 0, 1),
    d = Math.round((date1.valueOf() - date2.valueOf()) / 86400000);
  return Math.ceil((d + ((date2.getDay() + 1) - 1)) / 7);
};

// 生成标题
export const genReportTitle = () => {

  return `${DateUtil.getStartDayOfWeek()} 至 ${DateUtil.getEndDayOfWeek()}`

}