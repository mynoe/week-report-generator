import Vue from 'vue'
import App from './App.vue'
import ViewUI from 'iview'
import store from './store'
import 'iview/dist/styles/iview.css'
import Setters from './setter/index'
import componentList from './components/index'
import DataBase from './utils/db'

console.log('Setters', Setters, componentList)
Setters.map((item) => {
  Vue.component(item.name, item.component)
})

Vue.config.productionTip = false
Vue.prototype.$db = new DataBase({
  databaseName: 'reports',
  version: '2.6'
})
// Vue.prototype.$db.getReportList()

Vue.use(ViewUI)
new Vue({
  store,
  render: (h) => h(App),
}).$mount('#app')
