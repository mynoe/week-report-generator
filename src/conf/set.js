export default function MySet() {
  let items = {};

  this.add = function (val) {
    if (!this.has(val)) {
      items[val] = val
    }
    return false
  }

  this.has = function (value) {
    return value in items || Object.hasOwnProperty.call(items, value);
  }

  this.remove = function (val) {
    if (this.has(val)) {
      delete items.val
      return true
    }
    return false
  }

  this.clear = function () {
    items = {}
  }

  this.size = function () {
    return Object.keys(items).length;
  }
  this.values = function () {
    return Object.keys(items);
  };
  this.union = function (otherSet) {
    let unionSet = new Set();

    let values = this.values();

    for (let i = 0; i < values.length; i++) {
      unionSet.add(values[i]);
    }
    values = otherSet.values();

    for (let i = 0; i < values.length; i++) {
      unionSet.add(values[i]);
    }
    return unionSet;
  };
  this.intersection = function (otherSet) {
    let intersectionSet = new Set()
    let values = this.values()
    for (let i = 0; i < values.length; i++) {
      if (otherSet.has(values[i])) {
        intersectionSet.add(values[i])
      }
    }
    return intersectionSet
  }
  this.difference = function (otherSet) {
    let differenceSet = new Set()
    let values = this.values()
    for (let i = 0; i < values.length; i++) {
      if (!otherSet.has(values[i])) {
        differenceSet.add(values[i])
      }
    }
    return differenceSet
  }
  this.subset = function (otherSet) {
    if (this.size() > otherSet.size()) {
      return false
    } else {
      let values = this.values();
      for (let i = 0; i < values.length; i++) {
        if (!otherSet.has(values[i])) {
          return false;
        }
      }
      return true
    }
  }
}