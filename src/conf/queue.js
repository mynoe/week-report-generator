export default class Queue {
  constructor() {
    // this.count = 0;
    // this.lowestCount = 0;
    this.items = [];
  }

  enqueue(element) {
    // this.items[this.count] = element;
    // this.count++;
    this.items.push(element)
  }

  dequeue() {
    // if (this.isEmpty()) {
    //   return undefined;
    // }
    return this.items.shift()
    // const result = this.items[this.lowestCount];
    // delete this.items[this.lowestCount];
    // this.lowestCount++;
    // return result;
  }

  isEmpty() {
    return this.items.length === 0;
  }

  clear() {
    // this.items = {};
    // this.count = 0;
    // this.lowestCount = 0;
    this.items = []
  }

  size() {
    // return this.count - this.lowestCount;
    return this.items.length
  }

  toString() {
    if (this.isEmpty()) {
      return '';
    }
    let objString = `${this.items[this.lowestCount]}`;
    for (let i = this.lowestCount + 1; i < this.count; i++) {
      objString = `${objString},${this.items[i]}`;
    }
    return objString;
  }
}